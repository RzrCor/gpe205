﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class powerUp : MonoBehaviour
{
    [HideInInspector]
    public tankData Data;
    // Allows power up to be permanent
    public bool LastForever;
    // How long power up will last
    public float TimeToLast;
    // Tells if the power up was picked up
    private bool PowerUpActivated;
    // Time remaining for the power up to stay active
    private float TimeLeft;
    public Renderer Cube;
    public AudioSource Audio;

    // Update is called once per frame
    void Update()
    {
        // If power up is picked up
        if (PowerUpActivated == true)
        {
            // Start timer to deactivate power up
            TimeLeft = TimeLeft - Time.deltaTime;
            // If time runs out
            if (TimeLeft <= 0)
            {
                // Deactivate power up
                OnDeactivate();
            }
        }
    }
    public abstract void OnActivate();
    public abstract void OnDeactivate();
    public void OnTriggerEnter(Collider other)
    {
        if (PowerUpActivated == false)
        {
            //Gets tankData component
            var data = other.gameObject.GetComponent<tankData>();
            if (data != null)
            {
                Audio.Play(); //Plays audio queue for picking up the power up

                Data = data;
                Cube.enabled = false;
                OnActivate();
                PowerUpActivated = true;
                TimeLeft = TimeToLast;
                if (LastForever == true)
                {
                    OnDeactivate();
                }
            }
        }
    }
}
