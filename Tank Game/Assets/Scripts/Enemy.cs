﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int maxHealth = 100;
    public int curHealth = 100;
    // Damage done to enemy by bullet
    public int damage = 20;

    void Update()
    {
        if (curHealth < 1)
        {
            // Adds 10 points
            PointScript.points += 10;
            // Destroys enemy when health reaches 0
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            // Makes enemy health go down when hit by a bullet
            curHealth -= damage;
            // Destoys bullet when collids with the enemy
            Destroy(other.gameObject);
        }
    }
}
