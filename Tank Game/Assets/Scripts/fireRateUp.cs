﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireRateUp : powerUp
{ 
    //Holds the original fire rate speed
    private float originalFireRate;
    //Holds the increased fire rate speed
    public float powerUpFireRate;

    public override void OnActivate() // Activates on pick up
    {
        
        originalFireRate = Data.bulletDelay;
       
        Data.bulletDelay = powerUpFireRate;
    }

    public override void OnDeactivate() // Deactivates the power up
    {
        //Restores fire rate to original speed
        Data.bulletDelay = originalFireRate;
        //Destroys game object after pick up
        Destroy(gameObject);
    }
}
