﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tankData : MonoBehaviour
{
    private Player player;
    //Controls move speed
    public float moveSpeed; 
    //How quickly tank turns
    public float rotationSpeed = 90f; 
    //Controls firing speed
    public float fireRate;
    //How fast the bullet travels
    public float bulletSpeed;
    //How long it takes for a bullet to clear the game if target isn't hit.
    public float bulletDestroyTime;
    //Controls fire delay
    public float bulletDelay; 
    // Determins if AI is close enough to target
    public float closeEnoughDistance;
    //Controls how much health the tank has.
    public float tankHealth;
    //Controls how much damage the tank deals.
    public float tankDamage;
    //The player's score
    public int playerScore;
    //How many lives player has remaining
    public int playerLives;
    //Max health a tank can reach
    public float tankMaxHealth;


    private void Start()
    {
        player = GetComponent<Player>();
        if (player != null)
        {
            gameUI.Singleton[player.PlayerNumber].SetLives(playerLives);
        }
    }

    public void AddScore(int amount) //Keeps track of player's score
    {
        
        playerScore = playerScore + amount;
        if (player != null)
        {
            gameUI.Singleton[player.PlayerNumber].SetScore(playerScore);
        }
    }
    public void RemoveLife() //Reduces a life after a death
    {
        playerLives = playerLives - 1;
        if (playerLives == 0)
        {
            Destroy(gameObject);
        }
        if (player != null)
        {
            gameUI.Singleton[player.PlayerNumber].SetLives(playerLives);
        }
    }
}
