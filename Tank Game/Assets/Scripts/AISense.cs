﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISense : MonoBehaviour
{
 // hearing code


public GameObject Target;
    public float Range = 5f;

    public bool HearingPlayer()
    {
        return Vector3.Distance(Target.transform.position, transform.position) <= Range;
    }


// Sight code


  
    public float SightRange = 4f;
    public float FOV = 45f;
    public LayerMask CollisionLayers;

    public bool SeeingPlayer()
    {
        if (Target != null)
        {
            if (Vector3.Distance(Target.transform.position, transform.position) <= SightRange && Vector3.Angle(transform.right, Target.transform.position - transform.position) <= FOV)
            {
                var hit = Physics2D.Raycast(transform.position, (Target.transform.position - transform.position).normalized, SightRange, CollisionLayers);
                if (hit.collider != null && hit.collider.gameObject == Target)
                {
                    return true;
                }
            }
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
