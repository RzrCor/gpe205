﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speedPowerUp : powerUp
{
    //Holds the original speed
    private float originalSpeed;
    //Holds the increased speed
    public float powerUpSpeed;
    public override void OnActivate() // Activates on pick up
    {
        originalSpeed = Data.moveSpeed;
        Data.moveSpeed = powerUpSpeed;
    }

    public override void OnDeactivate() // Deactivates the power up
    {
        //Returns movement to original speed
        Data.moveSpeed = originalSpeed;
        // Destroys on pick up
        Destroy(gameObject);
    }
}