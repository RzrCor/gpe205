﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletMovement : MonoBehaviour
{
    // How fast bullet travels
    public float thrust = 150f;
    // Start is called before the first frame update
    void Start()
    {
        //Causes the bullet to go forward in any direction it is shot.
        GetComponent<Rigidbody>().AddForce(transform.forward * thrust, ForceMode.Impulse); 
    }

   
    
}
