﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tankMovement : MonoBehaviour
{

    public KeyCode Player1UpKey;
    public KeyCode Player1DownKey;
    public KeyCode Player1LeftKey;
    public KeyCode Player1RightKey;

    public KeyCode Player2UpKey;
    public KeyCode Player2DownKey;
    public KeyCode Player2LeftKey;
    public KeyCode Player2RightKey;

    private Player player;

    public CharacterController controller;

    private Vector3 moveDirection;
    // Allows designer to make the object have input controls or not
    public bool useInput;

    public tankData Data;

    void Start()
    {
        player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        controller.Move(moveDirection * Time.deltaTime);
        if (useInput == true)
        {
            Rotate(Data.rotationSpeed);
            Move(Data.moveSpeed);
        }
    }

    public void Rotate(float rotationSpeed)
    {

        //Lets our tank rotate using Unity's built in controls.
        float rotateTank = 1f;

        if (useInput == true)
        {
            rotateTank = 0;
            if (player.PlayerNumber == 1)
            {
                if (Input.GetKey(Player1LeftKey))
                {
                    rotateTank -= 1f;
                }
                if (Input.GetKey(Player1RightKey))
                {
                    rotateTank += 1f;
                }
            }
            else if (player.PlayerNumber == 2)
            {
                if (Input.GetKey(Player2LeftKey))
                {
                    rotateTank -= 1f;
                }
                if (Input.GetKey(Player2RightKey))
                {
                    rotateTank += 1f;
                }
            }
            //rotateTank = Input.GetAxis("Horizontal");
        }
        //Allows us to rotate.
        transform.Rotate(Vector3.up * Data.rotationSpeed * rotateTank * Time.deltaTime);
    }

    public void Move(float moveSpeed)
    {
        //Lets our tank move using Unity's built in controls.
        float moveTank = 1f;
        if (useInput == true)
        {
            //moveTank = Input.GetAxis("Vertical");
            moveTank = 0;
            if (player.PlayerNumber == 1)
            {
                if (Input.GetKey(Player1DownKey))
                {
                    moveTank -= 1f;
                }
                if (Input.GetKey(Player1UpKey))
                {
                    moveTank += 1f;
                }
            }
            else if (player.PlayerNumber == 2)
            {
                if (Input.GetKey(Player2DownKey))
                {
                    moveTank -= 1f;
                }
                if (Input.GetKey(Player2UpKey))
                {
                    moveTank += 1f;
                }
            }
        }
        //Allows us to move forward and backwards.

        moveDirection = (transform.forward * Data.moveSpeed * moveTank);

    }

    public void RotateTowards(Vector3 position, float rotationSpeed)
    {
        // Allows AI to rotate towards player
        var angle = Vector3.SignedAngle(transform.forward, position - transform.position, Vector3.up);
        if (angle > 0)
        {
            angle = Mathf.Clamp(angle, 0f, rotationSpeed);
        }
        if (angle < 0)
        {
            angle = -Mathf.Clamp(-angle, 0f, rotationSpeed);
        }
        transform.Rotate(Vector3.up * angle * Time.deltaTime);
    }
}
