﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherit from Controller
public class PlayerController : Controller
{
    public int maxHealth = 100;
    public int curHealth = 100;
    // Damage done to player per bullet 
    public int damage = 20;

    // Use this for initialization
    public override void Start()
	{
		// Call the Controller base class Start
		base.Start();
	}

	// Update is called once per frame
	void Update()
	{
		// The player controller gets input from the keyboard and then moves the pawn.
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
		{
			pawn.MoveForward();
		}

		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
		{
			pawn.MoveBackward();
		}

		// Only the person will rotate, but always handle the input
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
		{
			pawn.RotateLeft();
		}

		// Only the person pawn will rotate, but always handle the inpyut
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
		{
			pawn.RotateRight();

		}
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet);
            //bullet.transform.position = transform.position;
            //Debug.Log("test");
        }
		// All pawns can go home
		if (Input.GetKey(KeyCode.H))
		{
			pawn.GoHome();
		}

		if (Input.GetKey(KeyCode.R))
		{
			pawn.Roll();
		}
        if (curHealth < 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            // Makes sure you take damage when hit by an enemy bullet
            curHealth -= damage;
            // Destorys bullet
            Destroy(other.gameObject);
        }
        //if (other.gameObject.CompareTag("EnemyBullet"))
        {
            //Destroy(other.gameObject); // this destroys the enemy
            //Destroy(gameObject); // this destroys the bullet
        }
    }
}