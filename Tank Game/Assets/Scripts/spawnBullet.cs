﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBullet : MonoBehaviour
{
    public KeyCode Player1Fire;
    public KeyCode Player2Fire;
    public GameObject Bullet;
    private float bDelay;
    public bool useInput;
    public AudioSource Audio;
    [HideInInspector]

    private tankData Data;
    private Player player;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<Player>();
        // Retrieve component from tankData
        Data = GetComponentInParent<tankData>();

    }

    // Update is called once per frame
    void Update()
    {
        // If bullet delay is greater than 0
        if (bDelay > 0f)
        {
            // Start cooldown timer
            bDelay -= Time.deltaTime;
        }
        //Whenever the "Jump" function from unity is pressed
        // If bullet delay is less than or equal to 0
        if (bDelay <= 0 && useInput == true)
        {
            
            bool fire = false;
            if (player.PlayerNumber == 1)
            {
                fire = Input.GetKey(Player1Fire);
            }
            else if (player.PlayerNumber == 2)
            {
                fire = Input.GetKey(Player2Fire);
            }
            if (fire == true)
            {
                Audio.Play();// Plays audio of cannon firing
                //Causes the bullet prefab to spawn allowing it to be shot repeatedly.
                var newBullet = Instantiate(Bullet, transform.position, transform.rotation);
                newBullet.GetComponent<bulletDestroy>().data = Data;
                // Allows bullet delay to be adjusted in the inspector
                bDelay = Data.bulletDelay;
            }
        }


    }
    public void ShootBullet()
    {
        // if bullet delay is less than or equal to 0
        if (bDelay <= 0)
        {
            // Spawn bullet
            Instantiate(Bullet, transform.position, transform.rotation);
            bDelay = Data.bulletDelay;
        }

    }

}
