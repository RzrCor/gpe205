﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class numberDisplay : MonoBehaviour
{

    private string PreText;
    private Text text;
    private int value;


    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        PreText = text.text;
        text.text = PreText + "0";
    }

    
    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetNumber(int number)
    {
        
        text.text = PreText + number.ToString();
        value = number;
    }
    public int GetNumber()
    {
        return value;
    }
}
