﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    public Transform target;
    public float distance = 3.0f;
    public float height = 1.0f;
    public float damping = 5.0f;
    public bool smoothRotation = true;
    public bool followBehind = true;
    public float rotationDamping = 10.0f;
    public int PlayerNumber;
    //public static cameraFollow Singleton;
    public static Dictionary<int, cameraFollow> Singletons = new Dictionary<int, cameraFollow>();

    private new Camera camera;


    void Start()
    {
        camera = GetComponent<Camera>();
        //Singleton = this;
        Singletons.Add(PlayerNumber, this);
        Enable(false);
    }

    public void Enable(bool enable)
    {
        gameObject.SetActive(enable);
    }

    public void SetViewport(Rect viewport)
    {
        camera.rect = viewport;
    }

    void Update()
    {
        //Sets camera behind player
        if (target != null)
        {
            Vector3 wantedPosition;
            if (followBehind)
                wantedPosition = target.TransformPoint(0, height, -distance);
            else
                wantedPosition = target.TransformPoint(0, height, distance);

            transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * damping);

            if (smoothRotation)
            {
                Quaternion wantedRotation = Quaternion.LookRotation(target.position - transform.position, target.up);
                transform.rotation = Quaternion.Slerp(transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
            }
            else transform.LookAt(target, target.up);
        }
    }
}

