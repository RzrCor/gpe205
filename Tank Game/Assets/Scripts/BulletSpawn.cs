﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawn : MonoBehaviour
{
    public GameObject bullet;
    public Transform firePoint;
	public bool UseInput = true;

    public float enemyShotDelayCounter;

    private void FixedUpdate()
    {
        enemyShotDelayCounter -= Time.deltaTime;
		bool shoot = false;
		if (UseInput == true)
		{
			shoot = Input.GetButton("Jump"); // shoot is set to false once we press the space bar
		}
		else
		{
			shoot = true;
		}

        if (shoot)
        {
            if (enemyShotDelayCounter <= 0)
            {
                enemyShotDelayCounter = GameManager.instance.fireRate;
                Instantiate(bullet, firePoint.position, firePoint.rotation);       
            }
           
        }// shoot is now true and bullet shoots
    }
}
