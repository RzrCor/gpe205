﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public int PlayerNumber;
    public float InvincibilityTime = 3f;
    private float InvinicibiltyCounter;
    [HideInInspector]
    public Vector3 Spawnpoint;
    private Renderer[] renderers;
    private bool rendering = true;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.Singleton.Players.Add(this);
        renderers = GetComponentsInChildren<Renderer>();
        Spawnpoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (InvinicibiltyCounter > 0) //Gives the player an invincibility time after spawning from death
        {
            InvinicibiltyCounter -= Time.deltaTime;
            rendering = !rendering;
            for (int i = 0; i < renderers.GetLength(0); i++)
            {
                renderers[i].enabled = rendering;
            }
        }
        else if (rendering == false)
        {
            rendering = true;
            for (int i = 0; i < renderers.GetLength(0); i++)
            {
                renderers[i].enabled = rendering;
            }
        }
    }

    public bool IsInvincible()
    {
        return InvinicibiltyCounter > 0;
    }

    private void OnDestroy()
    {
        if (Application.isPlaying)
        {
            GameManager.Singleton.Players.Remove(this);
        }
    }
    public void MakeInvincible()
    {
        InvinicibiltyCounter = InvincibilityTime;
    }
}
