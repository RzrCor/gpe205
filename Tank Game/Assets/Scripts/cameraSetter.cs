﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraSetter : MonoBehaviour
{
    public Rect Player1Viewport = new Rect(0, 0f, 0.5f, 1f);
    public Rect Player2Viewport = new Rect(0.5f, 0, 1f, 1f);
    Player player;
    // Start is called before the first frame update
    void Start()
    {
        //Needed to set camera behind both players
        player = GetComponent<Player>();
        cameraFollow follower = cameraFollow.Singletons[player.PlayerNumber];
        int playerCount = mapManager.SpawnedPlayers;
        Debug.Log("Player Count = " + playerCount);
        follower.target = transform;
        follower.Enable(true);
        if (playerCount == 1)
        {
            follower.SetViewport(new Rect(0, 0, 1f, 1f));
        }
        else
        {
            if (player.PlayerNumber == 1)
            {
                follower.SetViewport(Player1Viewport);
            }
            else if (player.PlayerNumber == 2)
            {
                follower.SetViewport(Player2Viewport);
            }
        }
    }

  
}