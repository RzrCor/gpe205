﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public List<Transform> waypoints;
    public tankMovement movement;
    public tankData data;
    public float closeEnough = 1.0f;
    public float fleeDistance = 1.0f;
    private spawnBullet sBullet;

    public enum AIStates { Chase, Flee, Patrol, Sense }
    public AIStates aIStates = AIStates.Patrol;

    public enum LoopType { Loop, PingPong };
    public LoopType loopType = LoopType.Loop;

    public enum Sense { Idle, Chase, LookAround, GoHome }
    public Sense sense = Sense.LookAround;
    public AIStates currentState;

    public Transform target;
    public float avoidanceTime = 0.10f;

    private bool isPatrolForward = true;
    public int currentWaypoint = 0;
    private Transform tf;
    private float exitTime;
    private int avoidanceStage = 0;
    private Vector3 homePoint;
    private Vector3 goalPoint;

    private float rotationStorage = 0;

    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
        data = GetComponent<tankData>();
        movement = GetComponent<tankMovement>();
        homePoint = tf.position;
        sBullet = GetComponentInChildren<spawnBullet>();

    }

    // Update is called once per frame
    void Update()
    {
        if (aIStates == AIStates.Chase)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                Chase();
            }
        }

        if (aIStates == AIStates.Flee)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                Flee();
            }
        }

        if (aIStates == AIStates.Patrol)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                Patrol();
            }
        }
    }

    public void LoopLoop()
    {
        // Advance to the next waypoint, if we are still in range
        if (currentWaypoint < waypoints.Count - 1)
        {
            currentWaypoint++;
        }
        else
        {
            //Otherwise go to waypoint 0
            currentWaypoint = 0;
        }
    }

    public void LoopPingPong()
    {
        
        if (isPatrolForward)
        {
            // Advance to the next waypoint, if we are still in range
            if (currentWaypoint < waypoints.Count - 1)
            {
                currentWaypoint++;
            }
            else
            {
                //Otherwise reverse direction and decrement our current waypoint
                isPatrolForward = false;
                currentWaypoint--;
            }
        }
        else
        {
            // Advance to the next waypoint, if we are still in range
            if (currentWaypoint > 0)
            {
                currentWaypoint--;
            }
            else
            {
                //Otherwise reverse direction and increment our current waypoint
                isPatrolForward = true;
                currentWaypoint++;
            }
        }
    }


    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            //Get the current distance to the obstacle
            var ObstacleDistance = RaycastDistance(data.moveSpeed);
            //Try rotating left
            movement.Rotate(-2 * data.rotationSpeed);
            //Get the new distance
            var NewObstacleDistance = RaycastDistance(data.moveSpeed);
            movement.Rotate(2 * data.rotationSpeed);
            //If rotating left makes it worse...
            if (NewObstacleDistance < ObstacleDistance)
            {
                //Rotate right instead
                rotationStorage = 1f;
            }
            else
            {
                //Rotate left
                rotationStorage = -1f;
            }
            avoidanceStage = 2;
        }
        else if (avoidanceStage == 2)
        {
            //Continue to rotate until there is no obstacle in the way
            movement.Move(data.moveSpeed);
            movement.Rotate(rotationStorage * data.rotationSpeed);
            if (CanMove(data.moveSpeed))
            {
                rotationStorage = 0f;
                avoidanceStage = 0;
            }
        }
    }

    public float RaycastDistance(float distance)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hit something...
        if (Physics.Raycast(tf.position, tf.forward, out hit, distance))
        {
            return hit.distance;
        }
        return float.PositiveInfinity;
    }

    public bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hit something...
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            // ... and if what we hit is not the player...
            if (!hit.collider.CompareTag("Player"))
            {
                // ... then we can't move
                return false;
            }
        }
        // otherwise, we can move, so return true
        return true;
    }
    // Causes Enemy to chase player
    public void Chase()
    {
        if (CloseEnoughToTarget(data.closeEnoughDistance))
        {
            sBullet.ShootBullet();
        }
        movement.RotateTowards(target.position, data.rotationSpeed);

        if (CanMove(data.moveSpeed))
        {

            movement.Move(data.moveSpeed);
        }
        else
        {

            avoidanceStage = 1;
        }
    }

    public void Patrol()
    {

        /*if (movement.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
        {
             Do nothing!
        }
        else
        {
            // Move forward
            movement.Move(data.moveSpeed);
        }*/

        movement.RotateTowards(waypoints[currentWaypoint].position, data.rotationSpeed);

        if (CanMove(data.moveSpeed))
        {

            movement.Move(data.moveSpeed);
        }
        else
        {

            avoidanceStage = 1;
        }

        
        // If we are close to the waypoint,
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            
            switch (loopType)
            {
                case LoopType.Loop:
                    if (loopType == LoopType.Loop)
                    {
                        LoopLoop();
                    }
                    break;

                case LoopType.PingPong:
                    if (loopType == LoopType.PingPong)
                    {
                        LoopPingPong();
                    }
                    break;
            }
        }
    }

    public void Flee()
    {
        // The vector from ai to target is target position minus our position
        Vector3 vectorToTarget = target.position - tf.position;

        // we can flip the vector by -1 to get the vector AWAY from our target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // now, we can normalize that vector to give it a magnitute of 1
        vectorAwayFromTarget.Normalize();

        // a normalized vector can be multiplied by a length to make a vector of that length
        vectorAwayFromTarget *= fleeDistance;

        // we can find the position in space we want to move to by adding our vector away from our AI to our AI's position
        // this gives us a point that is "that vector away" from our current position
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        movement.RotateTowards(fleePosition, data.rotationSpeed);
        movement.Move(data.moveSpeed);
    }

    public void Idle()
    {
        // Do Nothing
    }

    public void GoHome()
    {
        goalPoint = homePoint;
        MoveTowards(goalPoint);
    }

    public void LookAround()
    {
        Turn(true);
    }

    public void MoveTowards(Vector3 target)
    {
        if (Vector3.Distance(tf.position, target) > closeEnough)
        {
            // Look at target
            Vector3 vectorToTarget = target - tf.position;
            tf.right = vectorToTarget;

            // Move Forward
            Move(tf.right);
        }
    }

    public void Move(Vector3 direction)
    {
        // Move in the direction passed in, at speed "moveSpeed"
        tf.position += (direction.normalized * data.moveSpeed * Time.deltaTime);
    }

    public void Turn(bool isTurnClockwise)
    {
        // Rotate based on turnSpeed and direction we are turning
        if (isTurnClockwise)
        {
            tf.Rotate(0, 0, data.rotationSpeed * Time.deltaTime);
        }
        else
        {
            tf.Rotate(0, 0, -data.rotationSpeed * Time.deltaTime);
        }
    }
    public bool CloseEnoughToTarget(float distance)
    {
        if (Vector3.Distance(target.position, transform.position) <= distance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}