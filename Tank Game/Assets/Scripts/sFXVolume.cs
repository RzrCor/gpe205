﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sFXVolume : volumeSlider
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnSliderChange(float value)
    {
        AudioController.SetSoundEffectsVolume(value); // Conrtols the SFX slider in the options menu
    }
}
