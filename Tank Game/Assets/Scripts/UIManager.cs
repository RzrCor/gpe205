﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{


    public GameObject MainMenuObject;
    public GameObject OptionsMenuObject;
    public AudioSource Audio;
    public Dropdown playerDropdown;
    // Start is called before the first frame update
    void Start()
    {
        MainMenuObject.SetActive(true);
        OptionsMenuObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToMainMenu() //Return to Main Menu from Options Menu
    {
        MainMenuObject.SetActive(true);
        OptionsMenuObject.SetActive(false);
        Audio.Play();
    }

    public void GoToOptionsMenu() //Go to options menu from Main Menu
    {
        MainMenuObject.SetActive(false);
        OptionsMenuObject.SetActive(true);
        Audio.Play();
    }

    public void PlayGame() //Leave Main Menu and enter game
    {

        PlayerPrefs.SetInt("PlayerMode", playerDropdown.value);
        SceneManager.LoadSceneAsync("SampleScene");
       
        Audio.Play();
    }
    
}
