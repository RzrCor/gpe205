﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameUI : MonoBehaviour
{

    public int PlayerNumber;
    public static Dictionary<int, gameUI> Singleton = new Dictionary<int, gameUI>();
    public numberDisplay ScoreDisplay;
    public numberDisplay LivesDisplay;
    public gameOverScreen GameOver;
    public AudioSource GameMusic;
    // Start is called before the first frame update
    void Start()
    {
        Singleton.Add(PlayerNumber, this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetScore(int score) //Displays score on screen
    {
        ScoreDisplay.SetNumber(score);
    }

    public void SetLives(int lives) //Displays lives left on screen
    {
        LivesDisplay.SetNumber(lives);
    }
    public void BeginScreen() //Displays the Menu screen
    {
        ScoreDisplay.gameObject.SetActive(true);
        LivesDisplay.gameObject.SetActive(true);
        GameOver.gameObject.SetActive(false);
    }

    public void DoGameOver() //Displays Game Over screen when lives run out
    {
        ScoreDisplay.gameObject.SetActive(false);
        LivesDisplay.gameObject.SetActive(false);
        GameOver.gameObject.SetActive(true);
        GameMusic.Stop();
        int CurrentHighscore = GetHighscore(); //Shows the highscore and your score on the Game Over screen
        int Score = ScoreDisplay.GetNumber();
        if (Score > CurrentHighscore)
        {
            CurrentHighscore = Score;
            SetHighscore(CurrentHighscore);
        }
        GameOver.ScoreDisplay.SetNumber(Score);
        GameOver.HighscoreDisplay.SetNumber(CurrentHighscore);
    }
    private int GetHighscore()
    {
        return PlayerPrefs.GetInt("HighscorePlayer" + PlayerNumber);
    }

    private void SetHighscore(int value)
    {
        PlayerPrefs.SetInt("HighscorePlayer" + PlayerNumber, value);
    }
}
