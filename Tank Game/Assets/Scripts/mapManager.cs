﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mapManager : MonoBehaviour
{
    // Array list of room prefabs
    public List<Room> RoomPrefabs;
    // The width of the room
    public float RoomWidth;
    // The height of the room
    public float RoomHeight;
    // How many rooms in the width direction
    public int mapWidth;
    // How many rooms in the height direction
    public int mapHeight;
    // Array list of enemy spawn points
    public List<GameObject> enemiesToSpawn;
    // Player spawn point
    public GameObject playerToSpawn;
    // Array list of power up spawns
    public List<GameObject> powerUpsToSpawn;

    public static int SpawnedPlayers = 0;
    // Start is called before the first frame update
    void Start()
    {
        SpawnedPlayers = 0;
        // List for all spawn points
        List<PlayerSpawnPoint> PlayerSpawnpoints = new List<PlayerSpawnPoint>();

        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                // Makes it so anywhere inside the map the doors are open but the outside edges are closed
               Room newRoom = Instantiate(RoomPrefabs[Random.Range(0, RoomPrefabs.Count)], new Vector3(x * RoomWidth, 0f,  y * RoomHeight), Quaternion.identity);
                if (x == 0)
                {
                    newRoom.doorWest.SetActive(true);
                }
                else
                {
                    newRoom.doorWest.SetActive(false);
                }
                if (x == mapWidth - 1)
                {
                    newRoom.doorEast.SetActive(true);
                }
                else
                {
                    newRoom.doorEast.SetActive(false);
                }
                if (y == mapHeight - 1)
                {
                    newRoom.doorNorth.SetActive(true);
                }
                else
                {
                    newRoom.doorNorth.SetActive(false);
                }
                if (y == 0)
                {
                    newRoom.doorSouth.SetActive(true);
                }
                else
                {
                    newRoom.doorSouth.SetActive(false);
                }
                // Calls the array list for patrol points
                var PatrolPoints = newRoom.GetComponentInChildren<PatrolPointList>();
                // Calls array list for enemy spawn points
                var spawnPoints = newRoom.GetComponentsInChildren<EnemySpawnPoint>();
                foreach (var spawnPoint in spawnPoints)
                {
                    // Spawns enemies at spawn points at random
                    var newEnemy = Instantiate(enemiesToSpawn[Random.Range(0, enemiesToSpawn.Count)], spawnPoint.transform.position, Quaternion.identity);
                    var EnemyAIController = newEnemy.GetComponent<AIController>();
                    if (EnemyAIController.aIStates ==AIController.AIStates.Patrol)
                    {
                        for (int i = 0; i < PatrolPoints.transform.childCount; i++)
                        {
                            EnemyAIController.waypoints.Add(PatrolPoints.transform.GetChild(i));
                        }
                    }
                }
                // Spawns the player at their spawn point
                var playerSpawnPoint = newRoom.GetComponentInChildren<PlayerSpawnPoint>();
                
                PlayerSpawnpoints.Add(playerSpawnPoint);
                var PowerUpSpawnPoints = newRoom.GetComponentsInChildren<PowerUpSpawnPoint>();
                foreach (var PowerUpSpawnPoint in PowerUpSpawnPoints)
                {
                    Instantiate(powerUpsToSpawn[Random.Range(0, powerUpsToSpawn.Count)], PowerUpSpawnPoint.transform.position, Quaternion.identity);
                }
            }
        }
        int playerMode = PlayerPrefs.GetInt("PlayerMode");
        void SpawnPlayer(int number)
        {
            var newPlayer = Instantiate(playerToSpawn, PlayerSpawnpoints[Random.Range(0, PlayerSpawnpoints.Count)].transform.position, Quaternion.identity).GetComponent<Player>();
            newPlayer.PlayerNumber = number;
            SpawnedPlayers = SpawnedPlayers + 1;
        }
        SpawnPlayer(1);
        if (playerMode == 1)
        {
            SpawnPlayer(2);
        } 
    }
    
}
