﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tankHealth : MonoBehaviour
{
    private tankData Data;
    private Player player;
    public AudioSource Audio;

    public void Attack(tankData source)
    {
       
        if (player == null || player.IsInvincible() == false)
        {
            // reduces health when hit
            Data.tankHealth = Data.tankHealth - Data.tankDamage;
            if (Data.tankHealth <= 0)
            {
                Data.RemoveLife();
                source.AddScore(100);
                if (Data.playerLives == 0)
                {
                    // Destroys object if below or equal to 0 health
                    Destroy(gameObject);
                    if (player != null)
                    {
                        Audio.Play(); //Plays player death sound
                        gameUI.Singleton[player.PlayerNumber].DoGameOver();
                    }
                }
                else
                {
                    Data.tankHealth = Data.tankMaxHealth;
                    if (player != null)
                    {
                        transform.position = player.Spawnpoint;
                        player.MakeInvincible();
                    }
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Gets component from tankData
        Data = GetComponent<tankData>();
        player = GetComponent<Player>();
        if (player != null)
        {
            gameUI.Singleton[player.PlayerNumber].BeginScreen();
        }
    }
}