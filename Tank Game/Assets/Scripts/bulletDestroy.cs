﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletDestroy : MonoBehaviour
{
    public tankData data;
    public AudioClip Audio;
    // Start is called before the first frame update
    void Start()
    {
        //Calls the bulletDestroy option in tankData
        Destroy(gameObject, data.bulletDestroyTime); 
    }


    private void OnCollisionEnter(Collision bullet)
    {
        //Causes the bullet to react to anything with the tag Wall
        if (bullet.gameObject.tag == "Wall") 
        {
           //Code needed for bullet to be destroyed
            Destroy(this.gameObject);
        }
        // If bullet hits an enemy or player
        if (bullet.gameObject.tag == "Enemy" || bullet.gameObject.tag == "Player")
        {
            // Pull data from tankHealth in tankData script
            var tankHealthComponent = bullet.gameObject.GetComponent<tankHealth>();
            // Deal damage to object
            tankHealthComponent.Attack(data);
            // Destroy bullet
            Destroy(gameObject);
            // Plays audio for bullet destruction
            AudioSource.PlayClipAtPoint(Audio, transform.position, AudioController.GetSoundEffectsVolume());
        }
    }
}
