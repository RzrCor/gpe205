﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doubleHealth : powerUp
{

    
    
    public override void OnActivate() // Activates on pick up
    {
        // Doubles the tanks health
        Data.tankHealth = Data.tankHealth * 2;
    }

    public override void OnDeactivate() // Deactivates the power up
    {
        // Destroys on pick up
        Destroy(gameObject);
    }
}
