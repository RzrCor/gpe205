﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        direction = GameManager.instance.player.transform.right;
        Destroy(this.gameObject, GameManager.instance.bulletLife); // bullet is destroyed after user desired set time limit
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction* GameManager.instance.bulletSpeed * Time.deltaTime); // bullets will move every frame
    }

    void OnTriggerEnter2D(Collider2D bullet) //  creates a trigger for the bullet
    {
        if (bullet.gameObject.tag == "Wall") // if the bullet makes contact with the wall tagged "Wall"
        {
            Destroy(this.gameObject);// Destroy the bullet
        }
    }
}