﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    private static AudioController Singleton;
    public AudioMixer MainMixer;

    private void Start()
    {
        Singleton = this;
    }

    public static float GetMasterVolume()
    {
        float result;
        Singleton.MainMixer.GetFloat("Master Volume",out result);
        return result;
    }

    public static float GetMusicVolume()
    {
        float result;
        Singleton.MainMixer.GetFloat("Music Volume", out result);
        return result;
    }

    public static float GetSoundEffectsVolume()
    {
        float result;
        Singleton.MainMixer.GetFloat("Sound Effects Volume", out result);
        return result;
    }

    public static void SetMasterVolume(float value)
    {
        Singleton.MainMixer.SetFloat("Master Volume", value);
    }

    public static void SetMusicVolume(float value)
    {
        Singleton.MainMixer.SetFloat("Music Volume", value);
    }

    public static void SetSoundEffectsVolume(float value)
    {
        Singleton.MainMixer.SetFloat("Sound Effects Volume", value);
    }

}
